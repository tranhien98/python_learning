





# tính diện tích hình

def square (param1):
    value = param1 * param1
    print("diện tích hình vuông là: " + str(value))


def rectangle (param1, param2):
    value = int(param1) * int(param2)
    print("diện tích hình chữ nhật là: " + str(value))


def triangle (param1, param2):
    value = (int(param1) * int(param2))/2
    print("diện tích hình tam giác là: " + str(value))


def main():
    print("-------------------Menu----------------------")
    print("----Bạn muốn chạy chương trình nào ?---------")
    print("----1-----Tính diện tích hình vuông----------")
    print("----2-----Tính diện tích hình chữ nhật-------")
    print("----3-----Tính diện tích hình tam giác-------")
    print("---------------------------------------------")
    program_run = int(input())
    if program_run == 1:
        do_dai = int(input("chiều dài cạnh: \n"))
        square(do_dai)
    elif program_run == 2:
        chieu_dai = int(input("chiều dài: \n"))
        chieu_rong = int(input("chiều rộng: \n"))
        rectangle(chieu_dai, chieu_rong)
    elif program_run == 3:
        chieu_dai = int(input("chiều cao: \n"))
        chieu_rong = int(input("độ dài đáy: \n"))
        rectangle(chieu_dai, chieu_rong)


if __name__ == '__main__':
    main()
